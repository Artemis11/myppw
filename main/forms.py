from django import forms

from .models import mataKuliah

class formMatkul(forms.ModelForm):
    class Meta:
        model = mataKuliah
        fields = {
            'nama',
            'dosen',
            'sks',
            'deskripsi',
            'smt_tahun',
            'ruangKelas',
        }
        labels = {
            'nama': 'Nama Mata Kuliah',
            'dosen': 'Dosen Pengajar',
            'sks': 'Jumlah SKS',
            'deskripsi': 'Deskripsi Mata Kuliah',
            'smt_tahun': 'Semester dan TA',
            'ruangKelas' : 'Ruang Kelas',
        }
