from django.db import models

# Create your models here.
class mataKuliah(models.Model):
    nama = models.CharField(max_length=50)
    dosen = models.CharField(max_length=50)
    sks = models.IntegerField()
    deskripsi = models.CharField(max_length=150)
    smt_tahun = models.CharField(max_length=50)
    ruangKelas = models.CharField(max_length=50)
