from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import formMatkul
from .models import mataKuliah


def home(request):
    return render(request, 'story3.html')

def showMatkul(request):
    form_matkul = formMatkul(request.POST or None)
    if (form_matkul.is_valid() and request.method == 'POST'):
        form_matkul.save()
        return HttpResponseRedirect('/')
    listmatkul = mataKuliah.objects.all()
    
    response = {
        'title': 'Input Data Mata Kuliah',
        'form_matkul': form_matkul,
        'listmatkul': listmatkul,
    }
    return render(request, 'forms.html', response)
