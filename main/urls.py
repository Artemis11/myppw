from django.urls import path

from . import views

app_name = 'myppw'

urlpatterns = [
    path('', views.home, name='story3'),
    path('daftarmatkul/', views.showMatkul, name='daftarmatkul' )
]
